# Block login

## EN
Blocks the entry to the user component from the frontend generating a redirect.


## ES
Bloquea el ingreso al componente de usuarios desde el frontend generando un redireccionamiento.